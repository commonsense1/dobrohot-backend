<?php

declare(strict_types = 1);

namespace App\Rpc\Responses\Volunteer;

use App\Rpc\Responses\ResponseInterface;
use Illuminate\Database\Eloquent\Collection;

class VolunteerCollectionResponse implements ResponseInterface
{
    /**
     * @var Collection
     */
    private Collection $collection;

    /**
     * @param Collection $collection
     */
    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->collection->toArray();
    }
}
