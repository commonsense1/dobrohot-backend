<?php

declare(strict_types = 1);

namespace App\Rpc\Requests\Prompt;

use App\Rpc\Requests\BaseRequest;

class AddPromptRequest extends BaseRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'quest_id' => 'required|integer',
            'comment'  => 'string|nullable',
        ];
    }
}
