<?php

declare(strict_types = 1);

namespace App\Rpc\Responses\Profile;

use App\Models\Organization;
use App\Rpc\Responses\ResponseInterface;

class OrganizationEntityResponse implements ResponseInterface
{
    /**
     * @var Organization
     */
    private Organization $organization;

    /**
     * @param Organization $organization
     */
    public function __construct(Organization $organization)
    {
        $this->organization = $organization;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->organization->toArray();
    }
}
