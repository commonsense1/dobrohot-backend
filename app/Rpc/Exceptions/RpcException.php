<?php

declare(strict_types = 1);

namespace App\Rpc\Exceptions;

use AvtoDev\JsonRpc\Errors\ErrorInterface;

/**
 * Base exception for rpc requests.
 */
class RpcException extends \RuntimeException implements ErrorInterface
{
    /**
     * @var array<string, mixed>|null
     */
    protected ?array $data;

    /**
     * @param string                    $message
     * @param int                       $code
     * @param array<string, mixed>|null $data
     */
    public function __construct(string $message = '', int $code = 0, ?array $data = null)
    {
        parent::__construct($message, $code, null);

        $this->data = $data;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        return $this->data;
    }
}
