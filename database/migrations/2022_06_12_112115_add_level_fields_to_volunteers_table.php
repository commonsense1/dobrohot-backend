<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLevelFieldsToVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('volunteers', function (Blueprint $table): void {
            $table->unsignedInteger('exp')->default(0)->nullable();
            $table->unsignedInteger('balance')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('volunteers', function (Blueprint $table): void {
            $table->dropColumn('exp');
            $table->dropColumn('balance');
        });
    }
}
