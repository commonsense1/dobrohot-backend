<?php

declare(strict_types = 1);

namespace App\Rpc\Controllers;

use App\Repositories\VolunteerRepo;
use App\Rpc\Requests\Volunteer\ListVolunteerRequest;
use App\Rpc\Responses\Volunteer\VolunteerCollectionResponse;

class VolunteerController
{
    /**
     * @param ListVolunteerRequest $request
     * @param VolunteerRepo        $repo
     *
     * @return VolunteerCollectionResponse
     */
    public function list(ListVolunteerRequest $request, VolunteerRepo $repo)
    {
        $collection = $repo->list($request->offset, $request->limit);

        return new VolunteerCollectionResponse($collection);
    }
}
