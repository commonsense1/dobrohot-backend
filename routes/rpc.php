<?php

use AvtoDev\JsonRpc\RpcRouter;
use App\Rpc\Controllers\TagController;
use App\Rpc\Controllers\LoginController;
use App\Rpc\Controllers\QuestController;
use App\Rpc\Controllers\PromptController;
use App\Rpc\Controllers\RegisterController;
use App\Rpc\Controllers\VolunteerController;
use App\Rpc\Controllers\Profile\VolunteerProfileController;
use App\Rpc\Controllers\Profile\OrganizationProfileController;

/*
|--------------------------------------------------------------------------
| RPC Routes
|--------------------------------------------------------------------------
|
| Here is where you can register rpc routes for your application.
|
*/

RpcRouter::on('organization.login', LoginController::class . '@loginOrganization');
RpcRouter::on('organization.register', RegisterController::class . '@registerOrganization');

RpcRouter::on('organization.profile.get', OrganizationProfileController::class . '@get');
RpcRouter::on('organization.profile.find', OrganizationProfileController::class . '@find');
RpcRouter::on('organization.profile.update', OrganizationProfileController::class . '@update');

RpcRouter::on('volunteer.login', LoginController::class . '@loginVolunteer');
RpcRouter::on('volunteer.register', RegisterController::class . '@registerVolunteer');

RpcRouter::on('volunteer.profile.get', VolunteerProfileController::class . '@get');
RpcRouter::on('volunteer.profile.find', VolunteerProfileController::class . '@find');
RpcRouter::on('volunteer.profile.update', VolunteerProfileController::class . '@update');
RpcRouter::on('volunteer.list', VolunteerController::class . '@list');

RpcRouter::on('quest.add', QuestController::class . '@add');
RpcRouter::on('quest.list', QuestController::class . '@list');

RpcRouter::on('quest.prompt.listByQuest', PromptController::class . '@listByQuest');
RpcRouter::on('quest.prompt.listByOrganization', PromptController::class . '@listByOrganization');
RpcRouter::on('quest.prompt.listByVolunteer', PromptController::class . '@listByVolunteer');
RpcRouter::on('quest.prompt.add', PromptController::class . '@add');
RpcRouter::on('quest.prompt.approve', PromptController::class . '@approve');
RpcRouter::on('quest.prompt.deny', PromptController::class . '@deny');
RpcRouter::on('quest.prompt.complete', PromptController::class . '@complete');

RpcRouter::on('tag.list', TagController::class . '@list');
