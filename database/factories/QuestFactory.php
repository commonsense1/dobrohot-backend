<?php

declare(strict_types = 1);

namespace Database\Factories;

use Carbon\Carbon;
use App\Models\Quest;
use App\Models\Organization;
use App\Enums\FormatInterface;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuestFactory extends Factory
{
    private const
        MIN_COINS_REWARD = 1,
        MAX_COINS_REWARD = 10,

        MIN_EXP_REWARD = 1,
        MAX_EXP_REWARD = 100,

        MIN_DAYS = 0,
        MAX_DAYS = 30;

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Quest::class;

    /**
     * Define the model's default state.
     *
     * @throws \Exception
     *
     * @return array
     */
    public function definition()
    {
        return [
            'organization_id' => Organization::all()->random()->id,
            'name'            => $this->faker->sentence,
            'description'     => $this->faker->sentence,
            'reward_coins'    => \random_int(self::MIN_COINS_REWARD, self::MAX_COINS_REWARD),
            'reward_exp'      => \random_int(self::MIN_EXP_REWARD, self::MAX_EXP_REWARD),
            'start_date'      => $this->calcRandomDateString(true),
            'end_date'        => $this->calcRandomDateString(),
            'format'          => $this->faker->randomElement(FormatInterface::AVAILABLE_FORMATS),
        ];
    }

    /**
     * @param bool $in_the_past
     *
     * @throws \Exception
     *
     * @return string
     */
    private function calcRandomDateString(bool $in_the_past = false): string
    {
        $date = Carbon::now();

        if ($in_the_past) {
            $date->subDays(\random_int(self::MIN_DAYS, self::MAX_DAYS));
        } else {
            $date->addDays(\random_int(self::MIN_DAYS, self::MAX_DAYS));
        }

        return $date->format('Y-m-d H:i:s');
    }
}
