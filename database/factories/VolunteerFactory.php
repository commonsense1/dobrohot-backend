<?php

declare(strict_types = 1);

namespace Database\Factories;

use App\Models\Volunteer;
use App\Enums\GenderInterface;
use App\Enums\VolunteerStatusInterface;
use Illuminate\Database\Eloquent\Factories\Factory;

class VolunteerFactory extends Factory
{
    private const
        MIN_SEED_BALANSE = 1,
        MAX_SEED_BALANCE = 10,

        MIN_SEED_EXP = 0,
        MAX_SEED_EXP = 1000;

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Volunteer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'last_name'  => $this->faker->lastName,
            'first_name' => $this->faker->firstName,
            'email'      => $this->faker->unique()->email,
            'phone'      => $this->faker->phoneNumber,
            'password'   => bcrypt('password'),

            'birthday'             => $this->faker->date,
            'gender'               => $this->faker->randomElement(GenderInterface::AVAILABLE_GENDERS),
            'vk_profile'           => $this->faker->url,
            'telegram_profile'     => $this->faker->url,
            'education'            => $this->faker->word,
            'job_place'            => $this->faker->address,
            'status'               => $this->faker->randomElement(VolunteerStatusInterface::AVAILABLE_STATUSES),
            'balance'              => \random_int(self::MIN_SEED_BALANSE, self::MAX_SEED_BALANCE),
            'exp'                  => \random_int(self::MIN_SEED_EXP, self::MAX_SEED_EXP),
        ];
    }
}
