<?php

declare(strict_types = 1);

namespace App\Repositories;

use App\Models\Tag;
use Illuminate\Database\Eloquent\Collection;

class TagRepo
{
    /**
     * @return Collection
     */
    public function listAll(): Collection
    {
        return Tag::all();
    }
}
