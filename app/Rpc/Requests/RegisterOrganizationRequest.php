<?php

declare(strict_types = 1);

namespace App\Rpc\Requests;

/**
 * @property string      $name
 * @property string      $email
 * @property string|null $phone
 * @property string      $password
 */
class RegisterOrganizationRequest extends BaseRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'name'     => 'required|string',
            'email'    => 'required|email|unique:organizations',
            'phone'    => 'string|nullable',
            'password' => 'required|min:1|confirmed',
        ];
    }
}
