<?php

declare(strict_types = 1);

namespace App\Rpc\Responses\Prompt;

use App\Enums\PromptStatusInterface;
use App\Rpc\Responses\ResponseInterface;

class ApprovePromptResponse implements ResponseInterface
{
    public function jsonSerialize()
    {
        return [
            'status' => PromptStatusInterface::APPROVED,
        ];
    }
}
