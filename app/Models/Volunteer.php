<?php

declare(strict_types = 1);

namespace App\Models;

use Carbon\Carbon;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property int         $id
 * @property string      $last_name
 * @property string      $first_name
 * @property string      $email
 * @property string      $phone
 * @property string      $password
 * @property Carbon|null $birthday
 * @property string|null $site
 * @property string|null $vk_profile
 * @property string|null $telegram_profile
 * @property string|null $job_place
 * @property string|null $vk_education
 * @property string|null $gender
 * @property string      $status
 * @property int         $exp
 * @property int         $balance
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Volunteer extends Authenticatable
{
    use HasApiTokens, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'last_name',
        'first_name',
        'email',
        'phone',
        'password',
        'birthday',
        'gender',
        'site',
        'vk_profile',
        'telegram_profile',
        'job_place',
        'education',
        'status',
        'exp',
        'balance',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array<string>
     */
    protected $hidden = [
        'password',
    ];

    /**
     * @return HasMany
     */
    public function prompts(): HasMany
    {
        return $this->hasMany(Prompt::class);
    }

    /**
     * @return BelongsToMany
     */
    public function skills(): BelongsToMany
    {
        return $this->belongsToMany(Skill::class);
    }
}
