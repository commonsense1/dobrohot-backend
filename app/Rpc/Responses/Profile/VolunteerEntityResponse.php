<?php

declare(strict_types = 1);

namespace App\Rpc\Responses\Profile;

use App\Models\Volunteer;
use App\Rpc\Responses\ResponseInterface;

class VolunteerEntityResponse implements ResponseInterface
{
    /**
     * @var Volunteer
     */
    private Volunteer $volunteer;

    /**
     * @param Volunteer $volunteer
     */
    public function __construct(Volunteer $volunteer)
    {
        $this->volunteer = $volunteer;
    }

    /**
     * @return array<mixed>
     */
    public function jsonSerialize(): array
    {
        return $this->volunteer->toArray();
    }
}
