<?php

declare(strict_types = 1);

namespace App\Rpc\Requests;

/**
 * @property string      $last_name
 * @property string      $first_name
 * @property string      $email
 * @property string|null $phone
 * @property string      $password
 */
class RegisterVolunteerRequest extends BaseRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'last_name'     => 'required|string',
            'first_name'    => 'required|string',
            'email'         => 'required|email|unique:volunteers',
            'phone'         => 'string|nullable',
            'password'      => 'required|min:1|confirmed',
        ];
    }
}
