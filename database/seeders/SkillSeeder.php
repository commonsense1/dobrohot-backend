<?php

namespace Database\Seeders;

use App\Models\Skill;
use Illuminate\Database\Seeder;

class SkillSeeder extends Seeder
{
    private const AVAILABLE_SKILLS = [
        'Первая помощь',
        'Поиск людей',
        'Иностранные языки',
        'Цифровые технологии',
        'Ораторство',
        'Стрессоустойчивость',
        'Организованность',
        'Многозадачность',
        'Коммуникабельность',
        'Лидерство',
        'Другое',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        foreach (self::AVAILABLE_SKILLS as $skill_name) {
            Skill::firstOrCreate(
                [
                    'name' => $skill_name,
                ]
            );
        }
    }
}
