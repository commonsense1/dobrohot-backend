<?php

declare(strict_types = 1);

namespace App\Rpc\Requests\Quest;

use App\Rpc\Requests\BaseRequest;

class AddQuestRequest extends BaseRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'organization_id' => 'required|integer|min:1',
            'name'            => 'required|string',
            'format'          => 'required|string',
            'reward_coins'    => 'required|integer|min:1',
            'reward_exp'      => 'required|integer|min:1',
            'start_date'      => 'required|string',
            'end_date'        => 'required|string',
        ];
    }
}
