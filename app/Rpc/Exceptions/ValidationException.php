<?php

declare(strict_types = 1);

namespace App\Rpc\Exceptions;

use AvtoDev\JsonRpc\Errors\ErrorInterface;

class ValidationException extends \InvalidArgumentException implements ErrorInterface
{
    protected const
        INVALID_ARGUMENT_MESSAGE = 'Invalid argument',
        INVALID_ARGUMENT_CODE    = -32602;

    /**
     * @var array|array<string, string>|null
     */
    private ?array $data;

    /**
     * ValidationException constructor.
     *
     * @param array|array<string, string>|null $data
     */
    public function __construct(?array $data = null)
    {
        parent::__construct(static::INVALID_ARGUMENT_MESSAGE, self::INVALID_ARGUMENT_CODE);

        $this->data = $data;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        return $this->data;
    }
}
