<?php

declare(strict_types = 1);

namespace App\Rpc\Requests\Profile;

use App\Rpc\Requests\BaseRequest;

class FindVolunteerRequest extends BaseRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'required|integer',
        ];
    }
}
