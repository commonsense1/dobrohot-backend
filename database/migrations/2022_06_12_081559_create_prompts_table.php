<?php

use App\Enums\PromptStatusInterface;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('prompts', function (Blueprint $table): void {
            $table->id();
            $table->string('comment')->nullable();
            $table->unsignedBigInteger('quest_id')->nullable();
            $table->unsignedBigInteger('volunteer_id')->nullable();
            $table->string('status', 16)->default(PromptStatusInterface::WAITING)->nullable();
            $table->timestamps();

            $table->foreign('quest_id')
                ->references('id')
                ->on('quests')
                ->onDelete('SET NULL');

            $table->foreign('volunteer_id')
                ->references('id')
                ->on('volunteers')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('prompts');
    }
}
