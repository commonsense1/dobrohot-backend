<?php

declare(strict_types = 1);

namespace App\Rpc\Requests;

/**
 * @property string $email
 * @property string $password
 */
class LoginOrganizationRequest extends BaseRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'email'    => 'required|email',
            'password' => 'required|string',
        ];
    }
}
