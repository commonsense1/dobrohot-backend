<?php

declare(strict_types = 1);

namespace App\Enums;

interface PromptStatusInterface
{
    public const
        WAITING   = 'waiting',
        APPROVED  = 'approved',
        DENIED    = 'denied',
        COMPLETED = 'completed';

    public const AVAILABLE_STATUSES = [
        self::WAITING,
        self::APPROVED,
        self::DENIED,
        self::COMPLETED,
    ];
}
