<?php

declare(strict_types = 1);

namespace Database\Seeders;

use App\Models\Organization;
use Illuminate\Database\Seeder;

class OrganizationSeeder extends Seeder
{
    private const COUNT_SEEDS = 10;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Organization::factory()
            ->count(self::COUNT_SEEDS)
            ->create();
    }
}
