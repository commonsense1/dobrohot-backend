<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('organizations', function (Blueprint $table): void {
            $table->string('inn')->nullable();
            $table->string('kpp')->nullable();
            $table->string('ogrn')->nullable();
            $table->string('address')->nullable();
            $table->string('site')->nullable();
            $table->string('vk_profile')->nullable();
            $table->string('telegram_profile')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('organizations', function (Blueprint $table): void {
            $table->dropColumn('inn');
            $table->dropColumn('kpp');
            $table->dropColumn('ogrn');
            $table->dropColumn('address');
            $table->dropColumn('site');
            $table->dropColumn('vk_profile');
            $table->dropColumn('telegram_profile');
        });
    }
}
