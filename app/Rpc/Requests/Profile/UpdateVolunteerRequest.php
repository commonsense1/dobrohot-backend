<?php

declare(strict_types = 1);

namespace App\Rpc\Requests\Profile;

use Illuminate\Validation\Rule;
use App\Rpc\Requests\BaseRequest;

class UpdateVolunteerRequest extends BaseRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'id'            => 'required|integer|min:1',
            'last_name'     => 'required|string',
            'first_name'    => 'required|string',
            'email'         => [
                'required',
                'email',
                Rule::unique('volunteers', 'email')->ignore($this->id),
            ],
            'phone'            => 'string|nullable',
            'birtday'          => 'date|nullable',
            'site'             => 'string|nullable',
            'gender'           => 'string|nullable',
            'vk_profile'       => 'string|nullable',
            'telegram_profile' => 'string|nullable',
            'education'        => 'string|nullable',
            'job_place'        => 'string|nullable',
            'status'           => 'string|nullable',
            'skills'           => 'array|nullable',
        ];
    }
}
