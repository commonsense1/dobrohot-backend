<?php

declare(strict_types = 1);

namespace Database\Seeders;

use App\Models\Quest;
use Illuminate\Database\Seeder;

class QuestSeeder extends Seeder
{
    private const COUNT_SEEDS = 50;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Quest::factory()
            ->count(self::COUNT_SEEDS)
            ->create();
    }
}
