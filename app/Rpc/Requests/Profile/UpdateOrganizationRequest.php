<?php

declare(strict_types = 1);

namespace App\Rpc\Requests\Profile;

use Illuminate\Validation\Rule;
use App\Rpc\Requests\BaseRequest;

class UpdateOrganizationRequest extends BaseRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'id'            => 'required|integer|min:1',
            'name'          => 'required|string',
            'email'         => [
                'required',
                'email',
                Rule::unique('organizations', 'email')->ignore($this->id),
            ],
            'phone'               => 'string|nullable',
            'address'             => 'string|nullable',
            'inn'                 => 'string|nullable',
            'kpp'                 => 'string|nullable',
            'ogrn'                => 'string|nullable',
            'site'                => 'string|nullable',
            'vk_profile'          => 'string|nullable',
            'telegram_profile'    => 'string|nullable',
            'skills'              => 'array|nullable',
        ];
    }
}
