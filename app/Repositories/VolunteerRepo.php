<?php

declare(strict_types = 1);

namespace App\Repositories;

use Carbon\Carbon;
use App\Models\Volunteer;
use Illuminate\Database\Eloquent\Collection;

class VolunteerRepo
{
    /**
     * @param int $id
     *
     * @return Volunteer
     */
    public function find(int $id): Volunteer
    {
        return Volunteer::findOrFail($id);
    }

    /**
     * Get Volunteer model by email or null if not exists.
     *
     * @param string $email
     *
     * @return Volunteer|null
     */
    public function getByEmail(string $email): ?Volunteer
    {
        return Volunteer::query()
            ->where('email', $email)
            ->first();
    }

    /**
     * Create organization.
     *
     * @param string      $last_name
     * @param string      $first_name
     * @param string      $email
     * @param string|null $phone
     * @param string      $password
     *
     * @return Volunteer
     */
    public function create(
        string $last_name,
        string $first_name,
        string $email,
        ?string $phone,
        string $password,
    ): Volunteer {
        return Volunteer::create(
            [
                'last_name'  => $last_name,
                'first_name' => $first_name,
                'email'      => $email,
                'phone'      => $phone,
                'password'   => bcrypt($password),
            ]
        );
    }

    /**
     * @param int         $id
     * @param string      $last_name
     * @param string      $first_name
     * @param string      $email
     * @param string|null $phone
     * @param Carbon|null $birthday
     * @param string|null $gender
     * @param string|null $site
     * @param string|null $vk_profile
     * @param string|null $telegram_profile
     * @param string|null $job_place
     * @param string|null $education
     * @param string|null $status
     * @param array       $skill_ids
     *
     * @return Volunteer
     */
    public function update(
        int $id,
        string $last_name,
        string $first_name,
        string $email,
        ?string $phone,
        ?Carbon $birthday,
        ?string $gender,
        ?string $site,
        ?string $vk_profile,
        ?string $telegram_profile,
        ?string $job_place,
        ?string $education,
        ?string $status,
        array $skill_ids = []
    ): Volunteer {
        /** @var Volunteer $volunteer */
        $volunteer = Volunteer::findOrFail($id);

        $volunteer->update(
            [
                'last_name'             => $last_name,
                'first_name'            => $first_name,
                'email'                 => $email,
                'phone'                 => $phone,
                'birthday'              => $birthday,
                'gender'                => $gender,
                'site'                  => $site,
                'vk_profile'            => $vk_profile,
                'telegram_profile'      => $telegram_profile,
                'job_place'             => $job_place,
                'education'             => $education,
                'status'                => $status,
            ]
        );

        $volunteer->skills()->sync($skill_ids);

        return $volunteer;
    }

    /**
     * @param int $offset
     * @param int $limit
     *
     * @return Collection
     */
    public function list(int $offset, int $limit): Collection
    {
        return Volunteer::query()
            ->offset($offset)
            ->limit($limit)
            ->get();
    }

    /**
     * @param int $id
     * @param int $coins
     * @param int $exp
     *
     * @return Volunteer
     */
    public function addReward(int $id, int $coins, int $exp): Volunteer
    {
        /** @var Volunteer $volunteer */
        $volunteer = Volunteer::findOrFail($id);
        $volunteer->balance += $coins;
        $volunteer->exp += $exp;
        $volunteer->save();

        return $volunteer;
    }
}
