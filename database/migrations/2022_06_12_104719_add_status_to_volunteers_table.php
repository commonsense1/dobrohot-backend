<?php

use Illuminate\Support\Facades\Schema;
use App\Enums\VolunteerStatusInterface;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('volunteers', function (Blueprint $table): void {
            $table->string('status')->default(VolunteerStatusInterface::AVAILABLE)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('volunteers', function (Blueprint $table): void {
            $table->dropColumn('status');
        });
    }
}
