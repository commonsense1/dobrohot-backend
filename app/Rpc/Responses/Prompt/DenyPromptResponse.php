<?php

declare(strict_types = 1);

namespace App\Rpc\Responses\Prompt;

use App\Enums\PromptStatusInterface;
use App\Rpc\Responses\ResponseInterface;

class DenyPromptResponse implements ResponseInterface
{
    public function jsonSerialize()
    {
        return [
            'status' => PromptStatusInterface::DENIED,
        ];
    }
}
