<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('volunteers', function (Blueprint $table): void {
            $table->date('birthday')->nullable();
            $table->string('gender', 16)->nullable();
            $table->string('vk_profile')->nullable();
            $table->string('telegram_profile')->nullable();
            $table->string('education')->nullable();
            $table->string('job_place')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('volunteers', function (Blueprint $table): void {
            $table->dropColumn('birthday');
            $table->dropColumn('gender');
            $table->dropColumn('vk_profile');
            $table->dropColumn('telegram_profile');
            $table->dropColumn('education');
            $table->dropColumn('job_place');
        });
    }
}
