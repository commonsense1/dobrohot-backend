<?php

declare(strict_types = 1);

namespace App\Enums;

interface GenderInterface
{
    public const
        FEMALE = 'male',
        MALE   = 'female';

    public const AVAILABLE_GENDERS = [
        self::FEMALE,
        self::MALE,
    ];
}
