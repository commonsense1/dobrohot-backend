<?php

declare(strict_types = 1);

namespace App\Rpc\Responses;

use Illuminate\Database\Eloquent\Model;

class SuccessfulAuthResponse implements ResponseInterface
{
    /**
     * @var string
     */
    private string $token;

    private Model $entity;

    /**
     * @param string $token
     * @param Model  $entity
     */
    public function __construct(string $token, Model $entity)
    {
        $this->token  = $token;
        $this->entity = $entity;
    }

    /**
     * @return string[]
     */
    public function jsonSerialize()
    {
        return [
            'token'  => $this->token,
            'entity' => $this->entity->toArray(),
        ];
    }
}
