<?php

declare(strict_types = 1);

namespace App\Enums;

interface FormatInterface
{
    public const
        ONLINE  = 'online',
        OFFLINE = 'offline';

    public const AVAILABLE_FORMATS = [
        self::ONLINE,
        self::OFFLINE,
    ];
}
