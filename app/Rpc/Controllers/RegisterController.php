<?php

declare(strict_types = 1);

namespace App\Rpc\Controllers;

use Illuminate\Auth\AuthManager;
use Illuminate\Support\Facades\DB;
use App\Repositories\VolunteerRepo;
use App\Rpc\Exceptions\RpcException;
use App\Repositories\OrganizationRepo;
use App\Rpc\Requests\RegisterVolunteerRequest;
use App\Rpc\Responses\RegisterVolunteerResponse;
use App\Rpc\Requests\RegisterOrganizationRequest;
use App\Rpc\Responses\RegisterOrganizationResponse;

class RegisterController
{
    /**
     * @param RegisterOrganizationRequest $request
     * @param OrganizationRepo            $organization_repo
     * @param AuthManager                 $auth
     *
     * @return RegisterOrganizationResponse
     */
    public function registerOrganization(
        RegisterOrganizationRequest $request,
        OrganizationRepo $organization_repo,
        AuthManager $auth,
    ): RegisterOrganizationResponse {
        DB::beginTransaction();

        try {
            $organization = $organization_repo->create(
                $request->name,
                $request->email,
                $request->phone,
                $request->password
            );

            $auth->guard('api-organizations')->setUser($organization);
            $token = $organization->createToken('Bearer');
        } catch (\Exception $exception) {
            DB::rollBack();

            throw new RpcException($exception->getMessage());
        }
        DB::commit();

        return new RegisterOrganizationResponse($organization, $token->plainTextToken);
    }

    /**
     * @param RegisterVolunteerRequest $request
     * @param VolunteerRepo            $volunteer_repo
     * @param AuthManager              $auth
     *
     * @return RegisterVolunteerResponse
     */
    public function registerVolunteer(
        RegisterVolunteerRequest $request,
        VolunteerRepo $volunteer_repo,
        AuthManager $auth,
    ): RegisterVolunteerResponse {
        DB::beginTransaction();

        try {
            $volunteer = $volunteer_repo->create(
                $request->last_name,
                $request->first_name,
                $request->email,
                $request->phone,
                $request->password
            );

            $auth->guard('api-volunteers')->setUser($volunteer);
            $token = $volunteer->createToken('Bearer');
        } catch (\Exception $exception) {
            DB::rollBack();

            throw new RpcException($exception->getMessage());
        }
        DB::commit();

        return new RegisterVolunteerResponse($volunteer, $token->plainTextToken);
    }
}
