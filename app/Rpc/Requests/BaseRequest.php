<?php

declare(strict_types = 1);

namespace App\Rpc\Requests;

use App\Rpc\Exceptions\ValidationException;
use Illuminate\Contracts\Validation\Factory;
use AvtoDev\JsonRpc\Requests\RequestInterface;

abstract class BaseRequest extends \stdClass
{
    /**
     * @var \stdClass
     */
    private \stdClass $request_params;

    /**
     * @var Factory
     */
    private Factory $factory;

    /**
     * @param RequestInterface $request
     * @param Factory          $factory
     */
    public function __construct(RequestInterface $request, Factory $factory)
    {
        $this->request_params = $request->getParams();
        $this->factory        = $factory;
        $this->validatesWhenResolved();
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->request_params->{$name} ?? null;
    }

    /**
     * @return array
     */
    abstract public function rules(): array;

    /**
     * Validates request after it has been resolved.
     */
    protected function validatesWhenResolved(): void
    {
        $validator = $this->factory->make(
            \json_decode(\json_encode($this->request_params), true),
            $this->rules()
        );

        if ($validator->fails()) {
            throw new ValidationException($validator->errors()->toArray());
        }
    }
}
