<?php

declare(strict_types = 1);

namespace App\Rpc\Exceptions;

use AvtoDev\JsonRpc\Errors\ErrorInterface;

class AuthException extends \InvalidArgumentException implements ErrorInterface
{
    protected const
        MESSAGE = 'Unauthenticated',
        CODE    = -32601;

    /**
     * @var array|array<string, string>|null
     */
    private ?array $data;

    /**
     * ValidationException constructor.
     *
     * @param array|array<string, string>|null $data
     */
    public function __construct(?array $data = null)
    {
        parent::__construct(static::MESSAGE, self::CODE);

        $this->data = $data;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        return $this->data;
    }
}
