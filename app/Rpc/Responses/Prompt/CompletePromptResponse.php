<?php

declare(strict_types = 1);

namespace App\Rpc\Responses\Prompt;

use App\Enums\PromptStatusInterface;
use App\Rpc\Responses\ResponseInterface;

class CompletePromptResponse implements ResponseInterface
{
    /**
     * @var int
     */
    private int $current_coins;

    /**
     * @var int
     */
    private int $current_exp;

    /**
     * @param int $current_coins
     * @param int $current_exp
     */
    public function __construct(int $current_coins, int $current_exp)
    {
        $this->current_coins = $current_coins;
        $this->current_exp   = $current_exp;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'status'        => PromptStatusInterface::COMPLETED,
            'current_coins' => $this->current_coins,
            'current_exp'   => $this->current_exp,
        ];
    }
}
