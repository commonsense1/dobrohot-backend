<?php

namespace Database\Factories;

use App\Models\Quest;
use App\Models\Volunteer;
use App\Enums\PromptStatusInterface;
use Illuminate\Database\Eloquent\Factories\Factory;

class PromptFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $quest     = Quest::all()->random();
        $volunteer = Volunteer::all()->random();

        return [
            'quest_id'        => $quest->id,
            'volunteer_id'    => $volunteer->id,
            'organization_id' => $quest->organization->id,
            'comment'         => $this->faker->text,
            'status'          => $this->faker->randomElement(PromptStatusInterface::AVAILABLE_STATUSES),
        ];
    }
}
