<?php

declare(strict_types = 1);

namespace App\Rpc\Controllers;

use Carbon\Carbon;
use App\Models\Organization;
use App\Repositories\QuestRepo;
use Illuminate\Auth\AuthManager;
use App\Rpc\Exceptions\AuthException;
use App\Rpc\Requests\Quest\AddQuestRequest;
use App\Rpc\Requests\Quest\ListQuestRequest;
use App\Rpc\Responses\Quest\AddQuestResponse;
use App\Rpc\Responses\Quest\ListQuestResponse;

class QuestController
{
    /**
     * @param AddQuestRequest $request
     * @param QuestRepo       $repo
     * @param AuthManager     $auth
     *
     * @return AddQuestResponse
     */
    public function add(AddQuestRequest $request, QuestRepo $repo, AuthManager $auth): AddQuestResponse
    {
        if (! $auth->guard('api-organizations')->check()) {
            throw new AuthException();
        }

        /** @var Organization $organization */
        $organization = $auth->guard('api-organizations')->user();

        $quest = $repo->add(
            $organization->id,
            $request->name,
            $request->description,
            $request->img,
            Carbon::parse($request->start_date),
            Carbon::parse($request->end_date),
            $request->reward_coins,
            $request->reward_exp,
            $request->format,
            $request->tags ?? []
        );

        return new AddQuestResponse($quest);
    }

    /**
     * @param ListQuestRequest $request
     * @param QuestRepo        $quest_repo
     *
     * @return ListQuestResponse
     */
    public function list(ListQuestRequest $request, QuestRepo $quest_repo)
    {
        $quest_collection = $quest_repo->listBy($request->offset, $request->limit);

        return new ListQuestResponse($quest_collection);
    }
}
