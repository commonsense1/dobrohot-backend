<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    private const AVAILABLE_TAGS = [
        'Здоровье и ЗОЖ',
        'Массовые мероприятия',
        'Природа и животные',
        'Помощь пожилым',
        'Дети и молодежь',
        'Поиск пропавших',
        'Культура и искусство',
        'Городская среда',
        'Права человека',
        'Социальная защита',
        'Наука и образование',
        'ЧС',
        'Другое',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        foreach (self::AVAILABLE_TAGS as $tag_name) {
            Tag::firstOrCreate(
                [
                    'name' => $tag_name,
                ]
            );
        }
    }
}
