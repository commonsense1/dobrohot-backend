<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('quest_tag', function (Blueprint $table): void {
            $table->id();
            $table->unsignedBigInteger('quest_id')->nullable();
            $table->unsignedBigInteger('tag_id')->nullable();
            $table->timestamps();

            $table->foreign('quest_id')
                ->references('id')
                ->on('quests')
                ->onDelete('SET NULL');

            $table->foreign('tag_id')
                ->references('id')
                ->on('tags')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('quest_tag');
    }
}
