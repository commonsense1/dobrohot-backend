<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::options('/rpc', function () {
    return 'Hello from RPC';
});

Route::withoutMiddleware(\App\Http\Middleware\VerifyCsrfToken::class)
    ->post('/rpc', '\AvtoDev\JsonRpc\Http\Controllers\RpcController')
    ->name('rpc.endpoint');
