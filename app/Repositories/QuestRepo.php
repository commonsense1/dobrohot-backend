<?php

declare(strict_types = 1);

namespace App\Repositories;

use Carbon\Carbon;
use App\Models\Quest;
use Illuminate\Database\Eloquent\Collection;

class QuestRepo
{
    /**
     * @param int         $organization_id
     * @param string      $name
     * @param string|null $description
     * @param string|null $img
     * @param Carbon      $start_date
     * @param Carbon      $end_date
     * @param int         $reward_coins
     * @param int         $reward_exp
     * @param string      $format
     * @param array       $tag_ids
     *
     * @return Quest
     */
    public function add(
        int $organization_id,
        string $name,
        ?string $description,
        ?string $img,
        Carbon $start_date,
        Carbon $end_date,
        int $reward_coins,
        int $reward_exp,
        string $format,
        array $tag_ids = []
    ): Quest {
        /** @var Quest $quest */
        $quest = Quest::create(
            [
                'organization_id' => $organization_id,
                'name'            => $name,
                'description'     => $description,
                'img'             => $img,
                'start_date'      => $start_date,
                'end_date'        => $end_date,
                'reward_coins'    => $reward_coins,
                'reward_exp'      => $reward_exp,
                'format'          => $format,
            ]
        );

        $quest->tags()->sync($tag_ids);

        return $quest;
    }

    /**
     * @param int $id
     *
     * @return Quest
     */
    public function find(int $id): Quest
    {
        return Quest::findOrFail($id);
    }

    /**
     * @param int $offset
     * @param int $limit
     *
     * @return Collection<Quest>
     */
    public function listBy(int $offset, int $limit)
    {
        return Quest::query()
            ->with('tags', 'organization')
            ->offset($offset)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get();
    }
}
