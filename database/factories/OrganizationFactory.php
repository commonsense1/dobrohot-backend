<?php

declare(strict_types = 1);

namespace Database\Factories;

use App\Models\Organization;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrganizationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Organization::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name'     => $this->faker->company,
            'email'    => $this->faker->unique()->email,
            'phone'    => $this->faker->phoneNumber,
            'password' => bcrypt('password'),

            'address'          => $this->faker->address,
            'site'             => $this->faker->url,
            'vk_profile'       => $this->faker->url,
            'telegram_profile' => $this->faker->url,
        ];
    }
}
