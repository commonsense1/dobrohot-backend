<?php

declare(strict_types = 1);

namespace App\Rpc\Controllers\Profile;

use App\Models\Organization;
use Illuminate\Auth\AuthManager;
use App\Rpc\Exceptions\AuthException;
use App\Repositories\OrganizationRepo;
use App\Rpc\Requests\Profile\FindOrganizationRequest;
use App\Rpc\Requests\Profile\UpdateOrganizationRequest;
use App\Rpc\Responses\Profile\OrganizationEntityResponse;

class OrganizationProfileController
{
    /**
     * @param AuthManager $auth
     *
     * @return OrganizationEntityResponse
     */
    public function get(AuthManager $auth): OrganizationEntityResponse
    {
        if (! $auth->guard('api-organizations')->check()) {
            throw new AuthException();
        }

        /** @var Organization $organization */
        $organization = $auth->guard('api-organizations')->user();
        $organization->load('skills');

        return new OrganizationEntityResponse($organization);
    }

    /**
     * @param FindOrganizationRequest $request
     * @param OrganizationRepo        $repo
     *
     * @return OrganizationEntityResponse
     */
    public function find(FindOrganizationRequest $request, OrganizationRepo $repo): OrganizationEntityResponse
    {
        $organization = $repo->find($request->id);
        $organization->load('skills');

        return new OrganizationEntityResponse($organization);
    }

    /**
     * @param UpdateOrganizationRequest $request
     * @param OrganizationRepo          $repo
     * @param AuthManager               $auth
     *
     * @return OrganizationEntityResponse
     */
    public function update(
        UpdateOrganizationRequest $request,
        OrganizationRepo $repo,
        AuthManager $auth
    ): OrganizationEntityResponse {
        if (! $auth->guard('api-organizations')->check()) {
            throw new AuthException();
        }

        $organization = $repo->update(
            $request->id,
            $request->name,
            $request->email,
            $request->phone,
            $request->address,
            $request->inn,
            $request->kpp,
            $request->ogrn,
            $request->site,
            $request->vk_profile,
            $request->telegram_profile,
            $request->skills ?? []
        );

        if ($request->skills !== null) {
            $organization->load('skills');
        }

        return new OrganizationEntityResponse($organization);
    }
}
