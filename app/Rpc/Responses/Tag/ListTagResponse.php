<?php

declare(strict_types = 1);

namespace App\Rpc\Responses\Tag;

use App\Rpc\Responses\ResponseInterface;
use Illuminate\Database\Eloquent\Collection;

class ListTagResponse implements ResponseInterface
{
    /**
     * @var Collection
     */
    private Collection $tags_collection;

    /**
     * @param Collection $tags_collection
     */
    public function __construct(Collection $tags_collection)
    {
        $this->tags_collection = $tags_collection;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->tags_collection->toArray();
    }
}
