<?php

declare(strict_types = 1);

namespace App\Rpc\Controllers;

use App\Models\Volunteer;
use App\Repositories\QuestRepo;
use App\Repositories\PromptRepo;
use Illuminate\Auth\AuthManager;
use Illuminate\Support\Facades\DB;
use App\Repositories\VolunteerRepo;
use App\Enums\PromptStatusInterface;
use App\Rpc\Exceptions\RpcException;
use App\Rpc\Exceptions\AuthException;
use App\Rpc\Requests\Prompt\AddPromptRequest;
use App\Rpc\Requests\Prompt\DenyPromptRequest;
use App\Rpc\Requests\Prompt\ListByQuestRequest;
use App\Rpc\Responses\Prompt\AddPromptResponse;
use App\Rpc\Responses\Prompt\DenyPromptResponse;
use App\Rpc\Requests\Prompt\ApprovePromptRequest;
use App\Rpc\Requests\Prompt\CompletePromptRequest;
use App\Rpc\Requests\Prompt\ListByVolunteerRequest;
use App\Rpc\Responses\Prompt\ApprovePromptResponse;
use App\Rpc\Responses\Prompt\CompletePromptResponse;
use App\Rpc\Requests\Prompt\ListByOrganizationRequest;
use App\Rpc\Responses\Prompt\PromptCollectionResponse;

class PromptController
{
    /**
     * @param AddPromptRequest $request
     * @param PromptRepo       $prompt_repo
     * @param AuthManager      $auth
     * @param QuestRepo        $quest_repo
     *
     * @return AddPromptResponse
     */
    public function add(
        AddPromptRequest $request,
        PromptRepo $prompt_repo,
        QuestRepo $quest_repo,
        AuthManager $auth
    ): AddPromptResponse {
        if (! $auth->guard('api-volunteers')->check()) {
            throw new AuthException();
        }

        /** @var Volunteer $volunteer */
        $volunteer = $auth->guard('api-volunteers')->user();
        $quest     = $quest_repo->find($request->quest_id);

        $prompt = $prompt_repo->add($volunteer->id, $quest->id, $quest->organization_id, $request->comment);

        return new AddPromptResponse($prompt);
    }

    /**
     * @param ListByOrganizationRequest $request
     * @param PromptRepo                $repo
     *
     * @return PromptCollectionResponse
     */
    public function listByOrganization(ListByOrganizationRequest $request, PromptRepo $repo): PromptCollectionResponse
    {
        $prompt_collection = $repo->listByOrganization(
            $request->organization_id,
            $request->offset,
            $request->limit
        );

        return new PromptCollectionResponse($prompt_collection);
    }

    /**
     * @param ListByVolunteerRequest $request
     * @param PromptRepo             $repo
     *
     * @return PromptCollectionResponse
     */
    public function listByVolunteer(ListByVolunteerRequest $request, PromptRepo $repo): PromptCollectionResponse
    {
        $prompt_collection = $repo->listByVolunteer(
            $request->volunteer_id,
            $request->offset,
            $request->limit
        );

        return new PromptCollectionResponse($prompt_collection);
    }

    /**
     * @param ListByQuestRequest $request
     * @param PromptRepo         $repo
     *
     * @return PromptCollectionResponse
     */
    public function listByQuest(ListByQuestRequest $request, PromptRepo $repo): PromptCollectionResponse
    {
        $prompt_collection = $repo->listByQuest($request->quest_id, $request->offset, $request->limit);

        return new PromptCollectionResponse($prompt_collection);
    }

    /**
     * @param ApprovePromptRequest $request
     * @param PromptRepo           $repo
     * @param AuthManager          $auth
     *
     * @return ApprovePromptResponse
     */
    public function approve(ApprovePromptRequest $request, PromptRepo $repo, AuthManager $auth): ApprovePromptResponse
    {
        // TODO: uncomment this
        /*if (! $auth->guard('api-organizations')->check()) {
            throw new AuthException();
        }*/

        $repo->setStatus($request->prompt_id, PromptStatusInterface::APPROVED);

        return new ApprovePromptResponse();
    }

    /**
     * @param DenyPromptRequest $request
     * @param PromptRepo        $repo
     * @param AuthManager       $auth
     *
     * @return DenyPromptResponse
     */
    public function deny(DenyPromptRequest $request, PromptRepo $repo, AuthManager $auth): DenyPromptResponse
    {
        // TODO: uncomment this
        /*        if (! $auth->guard('api-organizations')->check()) {
                    throw new AuthException();
                }*/

        $repo->setStatus($request->prompt_id, PromptStatusInterface::DENIED);

        return new DenyPromptResponse();
    }

    /**
     * @param CompletePromptRequest $request
     * @param PromptRepo            $prompt_repo
     * @param AuthManager           $auth
     * @param QuestRepo             $quest_repo
     * @param VolunteerRepo         $volunteer_repo
     *
     * @return CompletePromptResponse
     */
    public function complete(
        CompletePromptRequest $request,
        PromptRepo $prompt_repo,
        QuestRepo $quest_repo,
        VolunteerRepo $volunteer_repo,
        AuthManager $auth): CompletePromptResponse
    {
        // TODO: uncomment this
        /*        if (! $auth->guard('api-organizations')->check()) {
                    throw new AuthException();
                }*/

        DB::beginTransaction();

        try {
            $prompt    = $prompt_repo->setStatus($request->prompt_id, PromptStatusInterface::COMPLETED);
            $quest     = $quest_repo->find($prompt->quest_id);
            $volunteer = $volunteer_repo->addReward($prompt->volunteer_id, $quest->reward_coins, $quest->reward_exp);
        } catch (\Exception $exception) {
            DB::rollBack();

            throw new RpcException($exception->getMessage());
        }
        DB::commit();

        return new CompletePromptResponse($volunteer->balance, $volunteer->exp);
    }
}
