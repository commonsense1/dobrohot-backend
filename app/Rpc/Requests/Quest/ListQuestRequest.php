<?php

declare(strict_types = 1);

namespace App\Rpc\Requests\Quest;

use App\Rpc\Requests\BaseRequest;

/**
 * @property int $offset
 * @property int $limit
 */
class ListQuestRequest extends BaseRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'offset' => 'required|integer|min:0',
            'limit'  => 'required|integer|min:0|max:1000',
        ];
    }
}
