<?php

declare(strict_types = 1);

namespace App\Rpc\Controllers;

use Illuminate\Auth\AuthManager;
use App\Repositories\VolunteerRepo;
use App\Repositories\OrganizationRepo;
use Illuminate\Contracts\Hashing\Hasher;
use App\Rpc\Requests\LoginVolunteerRequest;
use Illuminate\Auth\AuthenticationException;
use App\Rpc\Responses\SuccessfulAuthResponse;
use App\Rpc\Requests\LoginOrganizationRequest;

class LoginController
{
    /**
     * Create auth token based on organization credentials.
     *
     * @param LoginOrganizationRequest $request
     * @param AuthManager              $auth
     * @param Hasher                   $hasher
     * @param OrganizationRepo         $organization_repo
     *
     *@throws AuthenticationException
     *
     * @return SuccessfulAuthResponse
     */
    public function loginOrganization(
        LoginOrganizationRequest $request,
        AuthManager $auth,
        Hasher $hasher,
        OrganizationRepo $organization_repo
    ): SuccessfulAuthResponse {
        $organization = $organization_repo->getByEmail($request->email);

        if (! $organization || ! $hasher->check($request->password, $organization->password)) {
            throw new AuthenticationException('Invalid credentials.');
        }

        $auth->guard('api-organizations')->setUser($organization);
        $token = $organization->createToken('Bearer');

        return new SuccessfulAuthResponse($token->plainTextToken, $organization);
    }

    /**
     * Create auth token based on volunteer credentials.
     *
     * @param LoginVolunteerRequest $request
     * @param AuthManager           $auth
     * @param Hasher                $hasher
     * @param VolunteerRepo         $volunteer_repo
     *
     *@throws AuthenticationException
     *
     * @return SuccessfulAuthResponse
     */
    public function loginVolunteer(
        LoginVolunteerRequest $request,
        AuthManager $auth,
        Hasher $hasher,
        VolunteerRepo $volunteer_repo
    ): SuccessfulAuthResponse {
        $volunteer = $volunteer_repo->getByEmail($request->email);

        if (! $volunteer || ! $hasher->check($request->password, $volunteer->password)) {
            throw new AuthenticationException('Invalid credentials.');
        }

        $auth->guard('api-volunteers')->setUser($volunteer);
        $token = $volunteer->createToken('Bearer');

        return new SuccessfulAuthResponse($token->plainTextToken, $volunteer);
    }
}
