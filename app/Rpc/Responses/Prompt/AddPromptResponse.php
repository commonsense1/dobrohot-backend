<?php

declare(strict_types = 1);

namespace App\Rpc\Responses\Prompt;

use App\Models\Prompt;
use App\Rpc\Responses\ResponseInterface;

class AddPromptResponse implements ResponseInterface
{
    /**
     * @var Prompt
     */
    private Prompt $prompt;

    /**
     * @param Prompt $prompt
     */
    public function __construct(Prompt $prompt)
    {
        $this->prompt = $prompt;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->prompt->toArray();
    }
}
