<?php

declare(strict_types = 1);

namespace App\Enums;

interface VolunteerStatusInterface
{
    public const
        AVAILABLE        = 'available',
        NOT_AVAILABLE    = 'not-available',
        PART_AVAILABLE   = 'part-available';

    public const AVAILABLE_STATUSES = [
        self::AVAILABLE,
        self::NOT_AVAILABLE,
        self::PART_AVAILABLE,
    ];
}
