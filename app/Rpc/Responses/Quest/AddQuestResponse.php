<?php

declare(strict_types = 1);

namespace App\Rpc\Responses\Quest;

use App\Models\Quest;
use App\Rpc\Responses\ResponseInterface;

class AddQuestResponse implements ResponseInterface
{
    /**
     * @var Quest
     */
    private Quest $quest;

    /**
     * @param Quest $quest
     */
    public function __construct(Quest $quest)
    {
        $this->quest = $quest;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->quest->toArray();
    }
}
