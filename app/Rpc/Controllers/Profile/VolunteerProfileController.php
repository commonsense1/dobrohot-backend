<?php

declare(strict_types = 1);

namespace App\Rpc\Controllers\Profile;

use Carbon\Carbon;
use App\Models\Volunteer;
use Illuminate\Auth\AuthManager;
use App\Repositories\VolunteerRepo;
use App\Rpc\Exceptions\AuthException;
use App\Rpc\Requests\Profile\UpdateVolunteerRequest;
use App\Rpc\Requests\Profile\FindOrganizationRequest;
use App\Rpc\Responses\Profile\VolunteerEntityResponse;

class VolunteerProfileController
{
    /**
     * @param AuthManager $auth
     *
     * @return VolunteerEntityResponse
     */
    public function get(AuthManager $auth): VolunteerEntityResponse
    {
        if (! $auth->guard('api-volunteers')->check()) {
            throw new AuthException();
        }

        /** @var Volunteer $volunteer */
        $volunteer = $auth->guard('api-volunteers')->user();
        $volunteer->load('skills');

        return new VolunteerEntityResponse($volunteer);
    }

    /**
     * @param FindOrganizationRequest $request
     * @param VolunteerRepo           $repo
     *
     * @return VolunteerEntityResponse
     */
    public function find(FindOrganizationRequest $request, VolunteerRepo $repo): VolunteerEntityResponse
    {
        $volunteer = $repo->find($request->id);
        $volunteer->load('skills');

        return new VolunteerEntityResponse($volunteer);
    }

    /**
     * @param UpdateVolunteerRequest $request
     * @param VolunteerRepo          $volunteer_repo
     * @param AuthManager            $auth
     *
     * @return VolunteerEntityResponse
     */
    public function update(
        UpdateVolunteerRequest $request,
        VolunteerRepo $volunteer_repo,
        AuthManager $auth
    ): VolunteerEntityResponse {
        if (! $auth->guard('api-volunteers')->check()) {
            throw new AuthException();
        }

        $volunteer = $volunteer_repo->update(
            $request->id,
            $request->last_name,
            $request->first_name,
            $request->email,
            $request->phone,
            $request->birthday !== null ? Carbon::parse($request->birthday) : null,
            $request->gender,
            $request->site,
            $request->vk_profile,
            $request->telegram_profile,
            $request->job_place,
            $request->education,
            $request->status,
            $request->skills ?? []
        );

        if ($request->skills !== null) {
            $volunteer->load('skills');
        }

        return new VolunteerEntityResponse($volunteer);
    }
}
