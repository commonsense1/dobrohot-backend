<?php

declare(strict_types = 1);

namespace Database\Seeders;

use App\Models\Volunteer;
use Illuminate\Database\Seeder;

class VolunteerSeeder extends Seeder
{
    private const COUNT_SEEDS = 10;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Volunteer::factory()
            ->count(self::COUNT_SEEDS)
            ->create();
    }
}
