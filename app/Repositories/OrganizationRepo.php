<?php

declare(strict_types = 1);

namespace App\Repositories;

use App\Models\Organization;

class OrganizationRepo
{
    /**
     * @param int $id
     *
     * @return Organization
     */
    public function find(int $id): Organization
    {
        return Organization::findOrFail($id);
    }

    /**
     * Get Organization model by email or null if not exists.
     *
     * @param string $email
     *
     * @return Organization|null
     */
    public function getByEmail(string $email): ?Organization
    {
        return Organization::query()
            ->where('email', $email)
            ->first();
    }

    /**
     * Create organization.
     *
     * @param string      $name
     * @param string      $email
     * @param string|null $phone
     * @param string      $password
     *
     * @return Organization
     */
    public function create(
        string $name,
        string $email,
        ?string $phone,
        string $password,
    ): Organization {
        return Organization::create(
            [
                'name'     => $name,
                'email'    => $email,
                'phone'    => $phone,
                'password' => bcrypt($password),
            ]
        );
    }

    /**
     * @param int         $id
     * @param string      $name
     * @param string      $email
     * @param string|null $phone
     * @param string|null $address
     * @param string|null $inn
     * @param string|null $kpp
     * @param string|null $ogrn
     * @param string|null $site
     * @param string|null $vk_profile
     * @param string|null $telegram_profile
     * @param array       $skill_ids
     *
     * @return Organization
     */
    public function update(
        int $id,
        string $name,
        string $email,
        ?string $phone,
        ?string $address,
        ?string $inn,
        ?string $kpp,
        ?string $ogrn,
        ?string $site,
        ?string $vk_profile,
        ?string $telegram_profile,
        array $skill_ids = []
    ): Organization {
        $organization = Organization::findOrFail($id);

        $organization->update(
            [
                'name'                    => $name,
                'email'                   => $email,
                'phone'                   => $phone,
                'address'                 => $address,
                'inn'                     => $inn,
                'kpp'                     => $kpp,
                'ogrn'                    => $ogrn,
                'site'                    => $site,
                'vk_profile'              => $vk_profile,
                'telegram_profile'        => $telegram_profile,
            ]
        );

        $organization->skills()->sync($skill_ids);

        return $organization;
    }
}
