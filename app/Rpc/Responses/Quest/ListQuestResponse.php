<?php

declare(strict_types = 1);

namespace App\Rpc\Responses\Quest;

use App\Rpc\Responses\ResponseInterface;
use Illuminate\Database\Eloquent\Collection;

class ListQuestResponse implements ResponseInterface
{
    /**
     * @var Collection
     */
    private Collection $quest_collection;

    /**
     * @param Collection $quest_collection
     */
    public function __construct(Collection $quest_collection)
    {
        $this->quest_collection = $quest_collection;
    }

    /**
     * @return array<mixed>
     */
    public function jsonSerialize(): array
    {
        return $this->quest_collection->toArray();
    }
}
