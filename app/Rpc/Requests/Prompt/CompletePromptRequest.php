<?php

declare(strict_types = 1);

namespace App\Rpc\Requests\Prompt;

use App\Rpc\Requests\BaseRequest;

class CompletePromptRequest extends BaseRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'prompt_id' => 'required|integer',
        ];
    }
}
