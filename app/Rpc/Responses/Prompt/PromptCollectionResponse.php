<?php

declare(strict_types = 1);

namespace App\Rpc\Responses\Prompt;

use App\Rpc\Responses\ResponseInterface;
use Illuminate\Database\Eloquent\Collection;

class PromptCollectionResponse implements ResponseInterface
{
    /**
     * @var Collection
     */
    private Collection $prompt_collection;

    /**
     * @param Collection $prompt_collection
     */
    public function __construct(Collection $prompt_collection)
    {
        $this->prompt_collection = $prompt_collection;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->prompt_collection->toArray();
    }
}
