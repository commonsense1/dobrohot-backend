<?php

declare(strict_types = 1);

namespace App\Repositories;

use App\Models\Prompt;
use App\Enums\PromptStatusInterface;
use Illuminate\Database\Eloquent\Collection;

class PromptRepo
{
    /**
     * @param int $volunteer_id
     * @param int $quest_id
     * @param $organization_id
     * @param string|null $comment
     *
     * @return Prompt
     */
    public function add(int $volunteer_id, int $quest_id, $organization_id, ?string $comment = null): Prompt
    {
        return Prompt::firstOrCreate(
            [
                'quest_id'        => $quest_id,
                'volunteer_id'    => $volunteer_id,
                'organization_id' => $organization_id,
            ],
            [
                'comment' => $comment,
            ]
        );
    }

    /**
     * @param int $quest_id
     * @param int $offset
     * @param int $limit
     *
     * @return Collection
     */
    public function listByQuest(int $quest_id, int $offset, int $limit): Collection
    {
        return Prompt::query()
            ->with(['volunteer', 'organization', 'quest'])
            ->where('quest_id', $quest_id)
            ->offset($offset)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    /**
     * @param int $organization_id
     * @param int $offset
     * @param int $limit
     *
     * @return Collection
     */
    public function listByOrganization(int $organization_id, int $offset, int $limit): Collection
    {
        return Prompt::query()
            ->with(['volunteer', 'organization', 'quest'])
            ->where('organization_id', $organization_id)
            ->offset($offset)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    /**
     * @param int $volunteer_id
     * @param int $offset
     * @param int $limit
     *
     * @return Collection
     */
    public function listByVolunteer(int $volunteer_id, int $offset, int $limit): Collection
    {
        return Prompt::query()
            ->with(['volunteer', 'organization', 'quest'])
            ->where('volunteer_id', $volunteer_id)
            ->offset($offset)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    /**
     * @param int    $id
     * @param string $status
     *
     * @return Prompt
     */
    public function setStatus(int $id, string $status): Prompt
    {
        if (! \in_array($status, PromptStatusInterface::AVAILABLE_STATUSES)) {
            throw new \InvalidArgumentException('Unknown status');
        }

        /** @var Prompt $prompt */
        $prompt = Prompt::findOrFail($id);

        // TODO: check quest belongs to organization

        $prompt->status = $status;
        $prompt->save();

        return $prompt;
    }
}
