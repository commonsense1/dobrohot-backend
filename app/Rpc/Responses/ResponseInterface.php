<?php

declare(strict_types = 1);

namespace App\Rpc\Responses;

interface ResponseInterface extends \JsonSerializable
{
}
