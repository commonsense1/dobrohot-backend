<?php

namespace Database\Seeders;

use App\Models\Prompt;
use Illuminate\Database\Seeder;

class PromptSeeder extends Seeder
{
    private const COUNT_SEEDS = 100;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Prompt::factory()
            ->count(self::COUNT_SEEDS)
            ->create();
    }
}
