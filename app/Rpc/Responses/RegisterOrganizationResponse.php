<?php

declare(strict_types = 1);

namespace App\Rpc\Responses;

use App\Models\Organization;

class RegisterOrganizationResponse implements ResponseInterface
{
    /**
     * @var Organization
     */
    private Organization $organization;

    /**
     * @var string
     */
    private string $token;

    /**
     * @param Organization $organization
     * @param string       $token
     */
    public function __construct(Organization $organization, string $token)
    {
        $this->organization = $organization;
        $this->token        = $token;
    }

    /**
     * @return array<mixed>
     */
    public function jsonSerialize(): array
    {
        return $this->organization->toArray() + ['token' => $this->token];
    }
}
