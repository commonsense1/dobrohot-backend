<?php

declare(strict_types = 1);

namespace App\Rpc\Responses;

use App\Models\Volunteer;

class RegisterVolunteerResponse implements ResponseInterface
{
    /**
     * @var Volunteer
     */
    private Volunteer $volunteer;

    /**
     * @var string
     */
    private string $token;

    /**
     * @param Volunteer $volunteer
     * @param string    $token
     */
    public function __construct(Volunteer $volunteer, string $token)
    {
        $this->volunteer = $volunteer;
        $this->token     = $token;
    }

    /**
     * @return array<mixed>
     */
    public function jsonSerialize(): array
    {
        return $this->volunteer->toArray() + ['token' => $this->token];
    }
}
