<?php

declare(strict_types = 1);

namespace App\Rpc\Requests\Prompt;

use App\Rpc\Requests\BaseRequest;

class ListByQuestRequest extends BaseRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'quest_id' => 'required|integer',
            'offset'   => 'required|integer|min:0',
            'limit'    => 'required|integer|min:0|max:1000',
        ];
    }
}
