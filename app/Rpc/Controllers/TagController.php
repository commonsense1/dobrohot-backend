<?php

declare(strict_types = 1);

namespace App\Rpc\Controllers;

use App\Repositories\TagRepo;
use App\Rpc\Responses\Tag\ListTagResponse;

class TagController
{
    /**
     * @param TagRepo $tag_repo
     *
     * @return ListTagResponse
     */
    public function list(TagRepo $tag_repo)
    {
        return new ListTagResponse($tag_repo->listAll());
    }
}
